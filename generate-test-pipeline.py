#!/usr/bin/python3
###################################################################################
# Apertis LAVA test pipeline generator
# Copyright (C) 2022 Collabora Ltd
# Ed Smith <ed.smith@collabora.com>

# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.

# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.

# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  US
###################################################################################

import argparse
import os
import yaml

parser = argparse.ArgumentParser(
    description='''Generate a Gitlab pipeline that will execute all
                   the Lava test files specified as arguments. Note
                   that this needs to know the current job and
                   pipeline, and that the test files need to be
                   artifacts of this job, so that each generated job
                   can retrieve its own test file.''')

parser.add_argument('-i', '--include', action='append', dest="includes", metavar="INCLUDE",
                    default=[],
                    help="a file to be listed in the includes section of the generated pipeline")
parser.add_argument('-o', '--output', type=argparse.FileType("w"), required=True, metavar="OUTPUT",
                    help="the pipeline filename to produce.")
parser.add_argument('-p', '--parent-pipeline', type=str, metavar="PARENT",
                    default=os.environ.get("CI_PIPELINE_ID"),
                    help="""the id of the Gitlab pipeline that will
                            trigger the test pipeline (defaults to
                            $CI_PIPELINE_ID)""")
parser.add_argument('-j', '--current-job-name', type=str, metavar="JOB",
                    default=os.environ.get("CI_JOB_NAME"),
                    help="""the name of the current job, which must
                            export the test files as artifacts (defaults to
                            $CI_JOB_NAME""")
parser.add_argument('-b', '--test-job-base', action='append', default=[],
                    help="""a gitlab job (usually a hidden job) that
                            the generated test jobs can extend);
                            this can be specified multiple times""")
parser.add_argument('test_files', metavar="TESTS", nargs='+',
                    help="Lava test files to be run in the pipeline")

args = parser.parse_args()

pipeline = { 'include': args.includes }

if args.current_job_name is None:
    print("error: CI_JOB_NAME is not set, and no --current-job-name given")
    exit(1)

test_source = {
    "job": f"{args.current_job_name}",
    "artifacts": True,
}

if args.parent_pipeline:
    test_source["pipeline"] = args.parent_pipeline

for f in args.test_files:
    job, _ = os.path.splitext(os.path.basename(f))
    pipeline[job] = {
        "extends": list(args.test_job_base),
        "needs": [ dict(test_source) ],
        "script": [ f"submit {os.path.basename(f)}" ],
    }

args.output.write(yaml.dump(pipeline))
